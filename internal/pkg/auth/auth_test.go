package auth_test

import (
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
)

const (
	credentialsUsername        = "test1"
	credentialsPassword        = "test1pwd"
	invalidCredentialsUsername = "test3"
	invalidCredentialsPassword = "test3pwd"
	userId                     = 1
)

var errValidateNotCalled error = errors.New("Validate must be called before determining token claims")

var validateSuccessfulCall bool

type validationMock struct {
	dontFindUser bool
}

func (v validationMock) Validate(crd auth.Credentials) bool {
	if crd.Username == credentialsUsername && crd.Password == credentialsPassword {
		validateSuccessfulCall = true
		return true
	}
	return false
}

func (v validationMock) GetClaims(crd auth.Credentials) (auth.Claims, error) {
	if !validateSuccessfulCall {
		return auth.Claims{}, errValidateNotCalled
	}
	if crd.Username == credentialsUsername && !v.dontFindUser {
		return auth.Claims{UserId: userId}, nil
	}
	return auth.Claims{}, auth.ErrUserNotFound
}

func TestNewAuthentication(t *testing.T) {
	authentication := auth.NewAuthentication([]byte(auth.DefaultJWTKey), auth.Validation{})
	want := "Authentication"
	got := reflect.TypeOf(*authentication).Name()
	if got != want {
		t.Fatalf("can not create authentication instance: want %v; got %v", want, got)
	}
	wantMethod := jwt.SigningMethodHS256
	gotMethod := authentication.Method()
	if !reflect.DeepEqual(wantMethod, gotMethod) {
		t.Fatalf("default method not set correctly: want %v; got %v", wantMethod, gotMethod)
	}
	wantDuration := auth.JWTDefaultDuration
	gotDuration := authentication.Duration()
	if gotDuration != wantDuration {
		t.Fatalf("duration incorrectly set: want %v; got %v", wantDuration, gotDuration)
	}
	wantSecretKey := []byte(auth.DefaultJWTKey)
	gotSecretKey := authentication.SecretKey()
	if !reflect.DeepEqual(wantSecretKey, gotSecretKey) {
		t.Fatalf("secret key incorrectly set: want %v; got %v", wantSecretKey, gotSecretKey)
	}
}

func TestSetMethod(t *testing.T) {
	authentication := auth.NewAuthentication([]byte(auth.DefaultJWTKey), auth.Validation{})
	want := jwt.SigningMethodES256
	authentication.SetMethod(want)
	got := authentication.Method()
	if !reflect.DeepEqual(want, got) {
		t.Errorf("can not set method correctly: want %v; got %v", want, got)
	}
}

func TestSetDuration(t *testing.T) {
	authentication := auth.NewAuthentication([]byte(auth.DefaultJWTKey), auth.Validation{})
	want := time.Duration(1 * time.Minute)
	authentication.SetDuration(want)
	got := authentication.Duration()
	if !reflect.DeepEqual(want, got) {
		t.Errorf("can not set duration correctly: want %v; got %v", want, got)
	}
}

func TestGenerateToken(t *testing.T) {
	authentication := auth.NewAuthentication([]byte(auth.DefaultJWTKey), validationMock{})
	crd := auth.Credentials{Username: credentialsUsername, Password: credentialsPassword}
	invalidCrd := auth.Credentials{Username: invalidCredentialsUsername, Password: invalidCredentialsPassword}

	t.Run("with invalid credentials", func(t *testing.T) {
		if _, _, err := authentication.GenerateToken(invalidCrd); !errors.Is(err, auth.ErrInvalidCredentials) {
			t.Fatal("validation passed with invalid credentials")
		}
	})

	t.Run("is GetClaims called only after successful validation", func(t *testing.T) {
		if _, _, err := authentication.GenerateToken(crd); errors.Is(err, errValidateNotCalled) {
			t.Fatal("method GetClaims must be called only after method Validate is called and response is positive")
		}
	})

	t.Run("claims can not find user", func(t *testing.T) {
		vMock := validationMock{dontFindUser: true}
		authentication := auth.NewAuthentication([]byte(auth.DefaultJWTKey), vMock)
		if _, _, err := authentication.GenerateToken(crd); !errors.Is(err, auth.ErrUserNotFound) {
			t.Fatalf("want %v; got %v", auth.ErrUserNotFound, err)
		}
	})

	t.Run("no errors when getting claims", func(t *testing.T) {
		if _, _, err := authentication.GenerateToken(crd); errors.Is(err, auth.ErrUserNotFound) {
			t.Fatalf("claims should be retrieved without errors: %v", err)
		}
	})

	t.Run("invalid secret key type", func(t *testing.T) {
		authentication := auth.NewAuthentication(auth.DefaultJWTKey, validationMock{})
		if _, _, err := authentication.GenerateToken(crd); err == nil {
			t.Fatal("invalid secret key type should not be overlooked", err)
		}
	})

	t.Run("token is not empty, not expired and there are no errors", func(t *testing.T) {
		authentication.SetDuration(1 * time.Hour)
		token, expires, err := authentication.GenerateToken(crd)
		if err != nil {
			t.Fatalf("token should be generated without errors: %v", err)
		}
		if time.Until(expires) <= 0 {
			t.Fatal("token unexpectedly expired")
		}
		if len(token) < 1 {
			t.Fatal("token should be generated non-empty")
		}
	})
}

func TestValidate(t *testing.T) {
	validation := auth.Validation{}
	tt := map[string]struct {
		crd  auth.Credentials
		want bool
	}{
		"Invalid username":              {crd: auth.Credentials{Username: invalidCredentialsUsername, Password: credentialsPassword}, want: false},
		"Invalid password":              {crd: auth.Credentials{Username: credentialsUsername, Password: invalidCredentialsPassword}, want: false},
		"Invalid username and password": {crd: auth.Credentials{Username: invalidCredentialsUsername, Password: invalidCredentialsPassword}, want: false},
		"Valid credentials":             {crd: auth.Credentials{Username: credentialsUsername, Password: credentialsPassword}, want: true},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			got := validation.Validate(tc.crd)
			if got != tc.want {
				t.Errorf("expected %v; got %v", tc.want, got)
			}
		})
	}
}

func TestGetClaims(t *testing.T) {
	crd := auth.Credentials{Username: invalidCredentialsUsername, Password: credentialsPassword}
	validation := auth.Validation{}
	if _, err := validation.GetClaims(crd); err == nil {
		t.Fatal("error expected when invalid credentials passed")
	}
	crd = auth.Credentials{Username: credentialsUsername, Password: credentialsPassword}
	claims, err := validation.GetClaims(crd)
	if err != nil {
		t.Fatalf("no error expected; got %v", err)
	}
	if claims.UserId != userId {
		t.Fatalf("expected %v; got %v", userId, err)
	}
}

func TestNewAuthorization(t *testing.T) {
	authorization := auth.NewAuthorization()
	want := "Authorization"
	got := reflect.TypeOf(*authorization).Name()
	if got != want {
		t.Fatalf("can not create authorization instance: want %v; got %v", want, got)
	}
}

func TestVerifyToken(t *testing.T) {
	secretKey := []byte(auth.DefaultJWTKey)
	crd := auth.Credentials{Username: credentialsUsername, Password: credentialsPassword}
	authentication := auth.NewAuthentication(secretKey, auth.Validation{})
	validToken, _, err := authentication.GenerateToken(crd)
	if err != nil {
		t.Fatal("token should be generated successfully")
	}
	authentication.SetDuration(-1 * time.Hour)
	expiredToken, _, err := authentication.GenerateToken(crd)
	if err != nil {
		t.Fatal("token should be generated successfully")
	}
	authorization := auth.NewAuthorization()
	tt := map[string]struct {
		token           string
		secretKey       interface{}
		want            *auth.Claims
		mustErrorsMatch bool
		expectNoErr     bool
		err             error
	}{
		"malformed token":      {token: "test-token", secretKey: secretKey, want: &auth.Claims{}, mustErrorsMatch: true, err: auth.ErrJWTInvalid},
		"with expired token":   {token: expiredToken, secretKey: secretKey, want: &auth.Claims{}, mustErrorsMatch: true, err: auth.ErrJWTInvalid},
		"secret keys mismatch": {token: validToken, secretKey: []byte("incorrect-key"), want: &auth.Claims{}, mustErrorsMatch: true, err: auth.ErrJWTInvalid},
		"valid claims":         {token: validToken, secretKey: secretKey, want: &auth.Claims{UserId: userId}, expectNoErr: true},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			got, err := authorization.VerifyToken(tc.token, tc.secretKey)
			if tc.mustErrorsMatch && !errors.Is(err, tc.err) {
				t.Errorf("expected error %v; got %v", tc.err, err)
			} else if tc.expectNoErr && err != nil {
				t.Errorf("expected no error; got %v", err)
			} else if got.UserId != tc.want.UserId {
				t.Errorf("expected %v; got %v", tc.want.UserId, got.UserId)
			}
		})
	}
}

func TestClaimsFromUnverifiedToken(t *testing.T) {
	authentication := auth.NewAuthentication([]byte(auth.DefaultJWTKey), auth.Validation{})
	token, _, err := authentication.GenerateToken(auth.Credentials{Username: credentialsUsername, Password: credentialsPassword})
	if err != nil {
		t.Fatal("token should be generated successfully")
	}
	authorization := auth.NewAuthorization()
	fakeClaims, err := authorization.ClaimsFromUnverifiedToken("fake-token")
	if fakeClaims != nil {
		t.Fatalf("expected claims as nil; got %v", fakeClaims)
	}
	if err == nil {
		t.Fatal("expected error; got nil")
	}
	claims, err := authorization.ClaimsFromUnverifiedToken(token)
	if err != nil {
		t.Fatalf("expected no error; got %v", err)
	}
	if claims.UserId != userId {
		t.Fatalf("expected userId %v; got %v", userId, claims.UserId)
	}
}

func TestParseTokenAsBearer(t *testing.T) {
	tt := map[string]struct {
		bearer string
		want   string
	}{
		"empty bearer":                                      {"", ""},
		"fake data instead of bearer":                       {"test ", ""},
		"fake data without space instead of bearer":         {"test", ""},
		"bearer without space":                              {"bearer", ""},
		"bearer without space with case-sensitive":          {"beArEr", ""},
		"bearer without token":                              {"bearer ", ""},
		"case sensitive bearer without token":               {"beAreR ", ""},
		"bearer with more than 2 parts":                     {"bearer bearer abc def", ""},
		"valid bearer":                                      {"bearer abc", "abc"},
		"valid bearer case-sensitive":                       {"BeArEr abc", "abc"},
		"valid bearer case-sensitive token":                 {"bearer aBc", "aBc"},
		"valid bearer with case-sensitive bearer and token": {"beAreR aBc", "aBc"},
		"token with trailing spaces":                        {"bearer  abcdef ", "abcdef"},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			got := auth.ParseTokenAsBearer(tc.bearer)
			if got != tc.want {
				t.Errorf("expected %v; got %v", tc.want, got)
			}
		})
	}
}
