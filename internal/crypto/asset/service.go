package asset

import (
	"fmt"

	"gitlab.com/justas2481/crypto-api/internal/crypto/asset/currency"
)

// AssetRepository interface abstracts the concrete implementation
// of a repository.
//
// That helps to separate
// actual storage logic from domain logic of an asset CRUD.
type AssetRepository interface {
	// Insert persists new asset assigned to particular user
	// and returns newly created asset ID.
	Insert(userId int64, asset Model) (id int64, err error)

	// Select obtains user assets from storage.
	//
	// Offset can be set to specify which row to read from
	// and limit specifies how many (maximum) rows to read.
	//
	// If limit = 0, all user assets have to be returned.
	//
	// Result rows can be ordered by map of fields.
	// These map of fields are key / value pairs that represent
	// fields names and specifies order ascending (true) / descending (false).
	Select(
		userId int64,
		offset, limit uint,
		orderByFields map[string]bool,
	) ([]Model, error)

	// Update persists updated asset.
	//
	// If error is returned as nil, updated asset persisted successfuly.
	Update(assetId int64, asset Model) error

	// Delete removes user's asset from storage.
	Delete(assetId int64, userId int64) error

	// SelectById returns an asset by its ID.
	//
	// Asset must belong to the given user by ID.
	SelectById(assetId int64, userId int64) (Model, error)
}

// currencyRatesExchanger abstracts the external API
// to be used for real-time currency exchange rates.
type currencyRatesExchanger interface {
	// RequestRates method requests an external API for rates
	// and internaly holds a model that represents actual rates.
	//
	// Only after RequestRates is called,
	// ExchangeRate can give an actual currency exchange rate
	// retrieved from external API.
	RequestRates() error

	// ExchangeRate returns currency exchange rate based on given currency type.
	//
	// It must previously be obtained
	// from an external resource by using RequestRates method.
	ExchangeRate(currency.Enum) (float64, error)
}

// Service uses repository to do certain actions against DB.
//
// It also deals with external resources that provide currency rates.
type Service struct {
	// Repository implements AssetRepository interface.
	repo AssetRepository

	// Service that contacts an external API for currency rates.
	exchangeRateService currencyRatesExchanger
}

// NewService initializes AssetService struct.
func NewService(repo AssetRepository, exchangeRateService currencyRatesExchanger) Service {
	return Service{repo, exchangeRateService}
}

// CreateAsset creates new user's asset and persists it in the staurage.
func (s Service) CreateAsset(userId int64, asset Model) (assetId int64, err error) {
	assetId, err = s.repo.Insert(userId, asset)
	if err != nil {
		return 0, fmt.Errorf("creating new asset: %w", err)
	}

	return assetId, nil
}

// GetAsset returns an asset by asset and user IDs given.
func (s Service) GetAsset(assetId int64, userId int64) (Model, error) {
	asset, err := s.repo.SelectById(assetId, userId)
	if err != nil {
		return asset, fmt.Errorf("obtaining asset: %w", err)
	}

	return asset, nil
}

// GetUserAssets gets all the assets that belong to the given user.
func (s Service) GetUserAssets(userId int64) ([]Model, error) {
	assets, err := s.repo.Select(userId, 0, 0, map[string]bool{"id": false})
	if err != nil {
		return assets, fmt.Errorf("getting user assets in service: %w", err)
	}

	return assets, nil
}

// UpdateAsset updates user asset.
func (s Service) UpdateAsset(userId, assetId int64, asset Model) error {
	asset.UserId = userId
	if err := s.repo.Update(assetId, asset); err != nil {
		return fmt.Errorf("asset service: %w", err)
	}

	return nil
}

// RemoveAsset Removes user asset.
func (s Service) RemoveAsset(assetId int64, userId int64) error {
	if err := s.repo.Delete(assetId, userId); err != nil {
		return fmt.Errorf("removeing asset: %w", err)
	}

	return nil
}

// ValueInUSD calculates given asset value in USD
// based on currently known currency rates.
func (s Service) ValueInUSD(userId int64, assetId int64) (currencyType string, currencyVal float64, err error) {
	asset, err := s.GetAsset(assetId, userId)
	if err != nil {
		return "", 0, err
	}
	if err = s.exchangeRateService.RequestRates(); err != nil {
		return "", 0, err
	}
	exchangeRate, err := s.exchangeRateService.ExchangeRate(*asset.Type)
	if err != nil {
		return "", 0, err
	}

	return string(*asset.Type), *asset.Amount * exchangeRate, nil
}

// SumValuesInUSD calculates sum of assets values in USD
// based on currently known currency rates and assets held by the user.
func (s Service) SumValuesInUSD(userId int64) (float64, error) {
	assets, err := s.GetUserAssets(userId)
	if err != nil {
		return 0, err
	}
	if err := s.exchangeRateService.RequestRates(); err != nil {
		return 0, err
	}
	var sum float64
	for _, asset := range assets {
		exchangeRate, err := s.exchangeRateService.ExchangeRate(*asset.Type)
		if err != nil {
			return 0, err
		}
		sum += *asset.Amount * exchangeRate
	}

	return sum, nil
}
