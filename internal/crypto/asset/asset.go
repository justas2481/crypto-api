// asset packages the Asset entity and domain logic of it.
package asset

import (
	"gitlab.com/justas2481/crypto-api/internal/crypto/asset/currency"
)

// Model represents Asset entity.
//
// User can CRUD her assets.
//
// Fields types are pointers to their actual datatypes.
// This helps to distinct 0 value from value not being set.
type Model struct {
	// Used as primary key.
	//
	// TODO: consider if it is worth to have
	// IDs as a separate type based on some primitive type.
	Id int64 `json:"id"`

	// User ID that Asset belongs to.
	UserId int64 `json:"user_id"`

	// Label for the asset.
	Label *string `json:"label"`

	// Type of a currency.
	Type *currency.Enum `json:"currency_type"`

	// Amount of a crypto currency owned.
	Amount *float64 `json:"currency_amount"`
}
