// main entry that manages authentication service.
package main

import (
	"log"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/justas2481/crypto-api/internal/auth/controller"
	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
	"gitlab.com/justas2481/crypto-api/internal/pkg/envconv"
	"gitlab.com/justas2481/crypto-api/internal/pkg/http"
)

func main() {
	log.Println("Starting auth service.")
	err := godotenv.Load("server.env")
	if err != nil {
		log.Fatal("Error: can not initialize server environment variables.")
	}
	err = godotenv.Load("../default_conf.env")
	if err != nil {
		log.Fatal("Error: can not initialize default configuration environment variables.")
	}

	router := mux.NewRouter().PathPrefix("/api").Subrouter()
	secretKey := []byte(envconv.GetString(os.Getenv("CRYPTO_JWT_KEY"), auth.DefaultJWTKey))
	authentication := auth.NewAuthentication(secretKey, auth.Validation{})
	dep := controller.NewDependencies(authentication)
	controller.SetRoutes(router, dep)

	server := http.NewServer(
		router,
		envconv.GetString(os.Getenv("HTTP_HOST"), http.DefaultServerAddress),
		envconv.GetString(os.Getenv("HTTP_PORT"), http.DefaultServerPort),
		time.Duration(envconv.GetInt(os.Getenv("HTTP_READ_HEADER_TIMEOUT"), http.DefaultServerReadHeaderTimeout)),
		time.Duration(envconv.GetInt(os.Getenv("HTTP_WRITE_TIMEOUT"), http.DefaultServerWriteTimeout)),
	)
	log.Fatal(server.Start())
}
