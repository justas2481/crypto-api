package asset

import "regexp"

// alphaUnderscore holds a helper function that checks if given string
// consists only of alpha and _.
// It is useful for validating table fields for ordering records.
var alphaUnderscore = regexp.MustCompile(`^[A-Za-z_]+$`).MatchString
