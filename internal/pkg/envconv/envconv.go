// envconv helps to deal with boilerplate code when obtaining environment variables
// and converting them to proper types.
//
// It also helps to assign default values to them if they can not be converted
// or if they are empty.
package envconv

import "strconv"

// GetString checks if given string is empty and if so,
// returns default, otherwise - the same given value.
func GetString(envVal, defaultVal string) string {
	if len(envVal) < 1 {
		return defaultVal
	}

	return envVal
}

// GetInt takes string value and converts it into proper int on success
// or returns default int value on failure.
func GetInt(envVal string, defaultVal int) int {
	val, err := strconv.Atoi(envVal)
	if err != nil {
		return defaultVal
	}

	return val
}
