// auth maintains authentication and authorization process.
//
// It uses JWTs to authenticate users by issuing signed tokens
// and checks their authorization by comparing user's token through request
// with encrypted header + claims + secret key (known to service / issuer only).
package auth

import (
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

// Errors.
var (
	ErrInvalidCredentials = errors.New("invalid credentials")
	ErrJWTInvalid         = errors.New("JWT is invalid")
	ErrUserNotFound       = errors.New("user can not be found")
)

// Configuration.
const (
	// For how long JWT is valid since being issued.
	JWTDefaultDuration time.Duration = 60 * time.Minute // 1 hour

	// DefaultJWTKey is mostly for testing,
	// but it works if there is no key in ENV.
	DefaultJWTKey = "d7FbAnqDr9{"
)

// Static credentials to authenticate.
//
// Since concurrent write read causes bad behaviour due to buckets in maps,
// concurrent access is insured through mutexes.
var staticCredentials map[string]struct {
	userId      int64
	credentials Credentials
}

// Mutex for concurrent access to the staticCredentials map.
var staticCredentialsMu sync.RWMutex

// To populate static credentials.
func init() {
	// TODO: reduce this boilerplate.
	staticCredentials = make(map[string]struct {
		userId      int64
		credentials Credentials
	})
	staticCredentials["test1"] = struct {
		userId      int64
		credentials Credentials
	}{userId: 1, credentials: Credentials{Username: "test1", Password: "test1pwd"}}
	staticCredentials["test2"] = struct {
		userId      int64
		credentials Credentials
	}{userId: 2, credentials: Credentials{Username: "test2", Password: "test2pwd"}}
}

// Credentials for authentication.
type Credentials struct {
	// Username of the user.
	Username string `json:"username"`

	// User's password (in plain string).
	Password string `json:"password"`
}

// Claims - info that will be stored in JWT.
//
// It can be anything that is needed though not secret info.
type Claims struct {
	// User's ID (mainly from some storage).
	UserId int64 `json:"user_id"`

	// Embedding other defaults to later be able to encode / decode data together
	// as default claims in JSON.
	jwt.RegisteredClaims
}

// Validater determines whether given user credentials are correct.
//
// It is also responsible for providing claims after successful validation.
type Validater interface {
	// Validate determines if given credentials are valid
	// so user can be authenticated.
	Validate(crd Credentials) bool

	// GetClaims returns claims after successful call to Validate method.
	GetClaims(Credentials) (Claims, error)
}

// Authentication holds all the data it needs to authenticate user
// by using JWTs.
type Authentication struct {
	// Private key that is used to issue signed tokens.
	secretKey interface{}

	// Validater that is used to determine whether
	// credentials are valid and for providing claims.
	validater Validater

	// method / algorithm for issuing tokens.
	method jwt.SigningMethod

	// For how long token must be valid.
	jwtDuration time.Duration
}

// NewAuthentication initializes Authentication.
//
// By default authentication uses jwt.SigningMethodHS256 to issue tokens.
//
// Token is valid for jwtDefaultDuration period by default.
func NewAuthentication(
	secretKey interface{},
	validater Validater,
) *Authentication {
	aut := &Authentication{
		secretKey: secretKey,
		validater: validater,
	}
	aut.SetMethod(jwt.SigningMethodHS256)
	aut.SetDuration(JWTDefaultDuration)

	return aut
}

// SetMethod sets signing method for authentication algorithm to be used.
func (a *Authentication) SetMethod(method jwt.SigningMethod) {
	a.method = method
}

// Method returns signing method for authentication algorithm
// that is currently used.
func (a *Authentication) Method() jwt.SigningMethod {
	return a.method
}

// SetDuration sets the time duration for how long token must be valid.
func (a *Authentication) SetDuration(duration time.Duration) {
	a.jwtDuration = duration
}

// Duration returns the time duration that is currently used to determine
// for how long token must be valid.
func (a *Authentication) Duration() time.Duration {
	return a.jwtDuration
}

// SecretKey returns a key that was used to sign JWT.
func (a *Authentication) SecretKey() interface{} {
	return a.secretKey
}

// GenerateToken issues JWT token if credentials are valid.
//
// Validater is used for validating credentials
// and for getting claims when credentials are valid.
//
// GetClaims method is called immediately after Validate method returns true.
//
// expires gives the expiration time of a token.
func (a *Authentication) GenerateToken(crd Credentials) (tokenString string, expires time.Time, err error) {
	if !a.validater.Validate(crd) {
		return "", time.Time{}, ErrInvalidCredentials
	}

	claims, err := a.validater.GetClaims(crd)
	if err != nil {
		return "", time.Time{}, err
	}

	expires = time.Now().Add(a.jwtDuration)
	claims.ExpiresAt = jwt.NewNumericDate(expires)
	token := jwt.NewWithClaims(a.method, claims)
	tokenString, err = token.SignedString(a.secretKey)
	if err != nil {
		return
	}

	return tokenString, expires, nil
}

// Validation for credentials.
type Validation struct{}

// Validate implements Validater interface.
func (v Validation) Validate(crd Credentials) bool {
	staticCredentialsMu.RLock()
	defer staticCredentialsMu.RUnlock()

	if val, ok := staticCredentials[crd.Username]; ok && crd == val.credentials {
		return true
	}

	return false
}

// GetClaims implements Validater interface.
func (v Validation) GetClaims(crd Credentials) (Claims, error) {
	staticCredentialsMu.RLock()
	defer staticCredentialsMu.RUnlock()

	if val, ok := staticCredentials[crd.Username]; ok {
		return Claims{UserId: val.userId}, nil
	}

	return Claims{}, ErrUserNotFound
}

// Authorization against given JWT token.
type Authorization struct {
	// Parser for JWT to check unverified tokens.
	parser *jwt.Parser
}

// New authorization initializes authorization struct.
func NewAuthorization() *Authorization {
	return &Authorization{jwt.NewParser()}
}

// VerifyToken Verifies if token is still valid and returns parsed claims.
//
// To verify if JWT is valid, it uses secret key
// which was used to issue the token during authentication.
//
// If error is returned as nil, token is valid.
func (a Authorization) VerifyToken(token string, secretKey interface{}) (*Claims, error) {
	claims := &Claims{}
	_, err := jwt.ParseWithClaims(token, claims, func(t *jwt.Token) (interface{}, error) {
		return secretKey, nil
	})
	if err != nil {
		return &Claims{}, ErrJWTInvalid
	}

	return claims, nil
}

// ClaimsFromUnverifiedToken.
//
// Helpful in handlers called from middleware to obtain claims.
func (a Authorization) ClaimsFromUnverifiedToken(token string) (*Claims, error) {
	claims := &Claims{}
	_, _, err := a.parser.ParseUnverified(token, claims)
	if err != nil {
		return nil, fmt.Errorf("parsing unverified token: %w", err)
	}

	return claims, nil
}

// A helper struct to quickly encode / decode token from JSON / to JSON.
type JSONToken struct {
	// Token itself.
	Token string `json:"token"`

	// When token expires.
	Expires time.Time `json:"expires"`
}

// ParseTokenAsBearer is a helper function
// used to extract JWT from Authorization header value.
func ParseTokenAsBearer(authorizationVal string) string {
	if authorizationVal == "" {
		return ""
	}
	if !strings.HasPrefix(strings.ToLower(authorizationVal), "bearer ") {
		return ""
	}
	parts := strings.Split(authorizationVal, string(authorizationVal[0:7]))
	if len(parts) != 2 {
		return ""
	}
	token := parts[1]
	token = strings.TrimSpace(token)

	return token
}
