package envconv_test

import (
	"reflect"
	"testing"

	"gitlab.com/justas2481/crypto-api/internal/pkg/envconv"
)

func TestGetString(t *testing.T) {
	tt := map[string]struct {
		envVal, defaultVal, want string
	}{
		"empty envVal":                           {envVal: "", defaultVal: "test", want: "test"},
		"envVal present":                         {envVal: "test", defaultVal: "test2", want: "test"},
		"empty both envVal and defaultVal":       {envVal: "", defaultVal: "", want: ""},
		"empty defaultVal and envVal is present": {envVal: "test", defaultVal: "", want: "test"},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			got := envconv.GetString(tc.envVal, tc.defaultVal)
			if !reflect.DeepEqual(got, tc.want) {
				t.Errorf("%v got; %v expected", got, tc.want)
			}
		})
	}
}

func TestGetInt(t *testing.T) {
	tt := map[string]struct {
		envVal           string
		defaultVal, want int
	}{
		"Regular int envVal":                            {envVal: "123", defaultVal: 0, want: 123},
		"envVal empty":                                  {envVal: "", defaultVal: 1, want: 1},
		"envVal non-convertable to int":                 {envVal: "test", defaultVal: 1, want: 1},
		"envVal pseudo-like hexadecimal representation": {envVal: "0xabcdef", defaultVal: 1, want: 1},
	}

	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			got := envconv.GetInt(tc.envVal, tc.defaultVal)
			if !reflect.DeepEqual(got, tc.want) {
				t.Errorf("%v got; %v expected", got, tc.want)
			}
		})
	}
}
