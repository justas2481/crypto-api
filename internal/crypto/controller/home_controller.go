package controller

import (
	"net/http"
)

// home controller.
type homeController struct{}

// home controller constructor.
func newHome() *homeController {
	return &homeController{}
}

// index handler.
func (h *homeController) index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"api_version": "v0.0.0"}`))
}
