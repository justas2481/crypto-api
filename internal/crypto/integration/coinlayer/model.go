package coinlayer

import (
	"errors"

	"gitlab.com/justas2481/crypto-api/internal/crypto/asset/currency"
)

// CurrencyRatesResponse it's the struct that represents currency rates
// that are returned from the external Coinlayer API.
type currencyRatesResponse struct {
	// Indicates if request was  successfull.
	Success bool `json:"success"`

	// List of currency exchange rates.
	Rates rates `json:"rates"`
}

// rates represents actual currency exchange rates.
type rates struct {
	BTC float64 `json:"BTC"`
	ETH float64 `json:"ETH"`
	LTC float64 `json:"LTC"`
}

// rate returns ratio in USD of a given currency type.
func (r rates) rate(cr currency.Enum) (float64, error) {
	switch cr {
	case currency.BTC:
		return r.BTC, nil
	case currency.ETH:
		return r.ETH, nil
	case currency.LTC:
		return r.LTC, nil
	default:
		return 0, errors.New("unsupported currency type")
	}
}
