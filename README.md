# Crypto API

This app is mainly an HTTP server that exposes RESTful API, which is responsible for
managing user assets. User can have different assets for different cryptocurrencies
(BTC, LTC and ETH are currently supported currencies), of which value in USD
can be obtained. Currency rates are taken by requesting the
[coinlayer.com](https://coinlayer.com/) service.
System contains two microservices (one for the authentication and another one for
the asset management itself). For simplicity, only the asset has its own CRUD and
users are left to be authenticated and authorized by using their static credentials. Code is extensively documented so to be easily reused in the future.
Although project is not completely covered by Unit or other types of tests, but
there is a black-boxing strategy for the tests applied.

## Prerequisites

This software is tested on Linux only and instructions are written how to run the
environment on Ubuntu operating system.

1. Install Go programming language by following these instructions at
[go.dev](https://go.dev/doc/install).
1. Install CLI for [Migrate](https://github.com/golang-migrate/migrate) library.
Installation instructions can be found
[here](https://github.com/golang-migrate/migrate/tree/master/cmd/migrate).
This tool is used to manage database migrations. Since this project currently uses
MySQL database, don't forget to install appropriate Migrate version.
1. Install MySQL database:
    1. Install MySQL server: `sudo apt install mysql-server`.
    1. Start managing MySQL with: `sudo mysql`.
    1. Use password as an authentication method for MySQL: 
    `ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 
    'your_password';`.
    1. Exit MySQL prompt: `exit`.
    1. Choose proper authentication method and set password: 
    `sudo mysql_secure_installation`.
    1. Connect to MYSql server as client: `sudo mysql -u root -p` and when prompted,
    enter the password that you've created.
    1. Create new database: `CREATE DATABASE database_name;`.
    1. Create a new MySQL user (because root can not be used with external apps):
    `CREATE USER 'your_username'@'localhost' IDENTIFIED BY 'your_password';`.
    1. Give privileges to the new user:
    `GRANT ALL PRIVILEGES ON *.* TO 'your_username'@'localhost' WITH GRANT OPTION;`.
    1. Flush privileges: `FLUSH PRIVILEGES;`.
    1. Exit the MySQL CLI: `exit`.  
    If you still need help installing MySQL, you can read
    [this guide](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-22-04).
1. Add MySQL credentials to
`crypto-api/cmd/crypto/mysql.env.example`
and rename `mysql.env.example` to `mysql.env`.
1. Head over to
`crypto-api/internal/crypto/db/mysql`
and execute database migrations:
`migrate -database "mysql://mysql_user:mysql_password@tcp(127.0.0.1:3306)/db_name" -path migrations up`.
1. Move to the project route - AKA `crypto-api`.
1. Run `go mod tidy` to download project dependencies.
1. Run tests: `go test ./...`.

## Auth microservice

Auth microservice must be requested to obtain JSON Web Token (AKA JWT).
Valid user credentials must be provided to the service to obtain the token and that
same token should be used to interact with the asset service. Token must be provided
by the client in every request (via Authorization header).

### Building and running

1. Head over to `crypto-api/cmd` directory and open `default_conf.env.example` file.
1. Create some `CRYPTO_JWT_KEY` which is shared between services as an environment
variable to issue and validate JWTs + decode JWT claims.
1. Rename this file to `default_conf.env`.
1. Move to `crypto-api/cmd/auth` and open `server.env.example` file.
1. Configure server host, port ant etc. Configurations should be self-explanatory.
1. Rename this file to `server.env`.
1. Issue `go build .` command to build the binary.
1. Run the service by issuing `./auth`.

### Requesting the JWT

You need to do an HTTP POST request by providing user credentials. There are
currently two static users to test. CURL request example:

```
curl -X POST \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-d '{"username": "test1", "password": "test1pwd"}' \
http://localhost:8080/api/authentication
```

For a second user, just change request body as JSON to:

```
-d '{"username": "test2", "password": "test2pwd"}'
```

Response example:  
**{"token":"eyJhbG...","expires":"2023-05-07T23:28:04.95955+03:00"}**

## Asset microservice

Asset microservice is responsible for running an actual API itself.

### Building and running

1. Head over to **crypto-api/cmd/crypto** directory.
1. In **server.env.example**, set your server host, port and etc.
1. Rename **server.env.example** to **server.env**.
1. In **coinlayer.env.example** file, add your own
[Coinlayer](https://coinlayer.com/) API key and rename file to **coinlayer.env**.
1. Issue `go build .` command to build the binary.
1. Run the service by issuing `./crypto`.

### Assigning an Asset to the user

To assign an asset to the user, issue POST request as such:

```
curl -X POST \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-H 'Authorization: BEARER ABC123-token-here...' \
-d '{"label": "my awesome currency", "currency_type": "btc", "currency_amount": 0.25}' \
http://localhost:8000/api/assets
```

Example response:  
**{"asset_id":1}**

### Retrieve all user assets

Example request:

```
curl -X GET \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-H 'Authorization: BEARER {token_here}' \
http://localhost:8000/api/assets
```

Example response:  
**[
{"id":1,"user_id":1,"label":"my awesome currency","currency_type":"btc","currency_amount":0.25},
{"id":2,"user_id":1,"label":"My asset","currency_type":"ltc","currency_amount":1.2321},
{"id":3,"user_id":1,"label":"My favourite","currency_type":"eth","currency_amount":3.721}
]**

### Retrieve info about Asset by ID

To retreave particular asset's value in $, you need to request this route:
**/api/assets/{asset_id}/value**.

Example request:

```
curl -X GET \
-H 'Accept: application/json' \
-H 'Authorization: BEARER '{token}' \
http://localhost:8000/api/assets/1/value
```

Example response:  
**{"name":"BTC","val":5597.1773}**

### Retrieve all assets values in $

You need to request this route:
**/api/assets/value**.

Example request:

```
curl -X GET \
-H 'Accept: application/json' \
-H 'Authorization: BEARER '{token}' \
http://localhost:8000/api/assets/value
```

Example response:  
**{"total":7270.58816744}**

### Update asset

To update asset, make a PATCH request to route: **/api/assets/{asset_id}**.

Example request:

```
curl -X PATCH \
-H 'Accept: application/json' \
-H 'Content-Type: application/json' \
-H 'Authorization: BEARER {token}' \
-d '{"currency_amount": 0.01}' \
http://localhost:8000/api/assets/1
```

Example response:  
**null**

Status 204 NO-CONTENT also indicates the successful action.

### Delete asset

To delete asset, make a DELETE request to **/assets/{asset_id}** endpoint.