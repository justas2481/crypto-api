// coinlayer its the package for
// external real-time currency exchange rates API integration.
package coinlayer

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/justas2481/crypto-api/internal/crypto/asset/currency"
	"gitlab.com/justas2481/crypto-api/internal/pkg/http"
)

const (
	// URL for the API starting endpoint.
	ratesEndpointUrl = "http://api.coinlayer.com/"

	// methodRates for getting currency rates.
	methodRates = "live"
)

// Service makes requests to the Coinlayer API endpoint
// and obtains real-time currencies rates.
type Service struct {
	client *http.Client
	apiKey string // API key.

	// Internally holds obtainned currency rates.
	rates currencyRatesResponse
}

// NewService returns new instance of the service.
func NewService(client *http.Client, apiKey string) *Service {
	return &Service{client: client, apiKey: apiKey}
}

// RequestRates method requests an external Coinlayer API for rates
// and internaly holds a model that represents actual rates.
//
// It implements asset.currencyRatesExchanger interface.
func (cs *Service) RequestRates() error {
	url := ratesEndpointUrl + methodRates + "?access_key=" + cs.apiKey
	body, err := cs.client.Get(url)
	if err != nil {
		return fmt.Errorf("making an HTTP request to the Coinlayer API: %w", err)
	}
	if err := json.Unmarshal(body, &cs.rates); err != nil {
		return fmt.Errorf("decoding response from Coinlayer API: %w", err)
	}
	if !cs.rates.Success {
		return errors.New("unsuccessful atempt to get currency rates")
	}

	return nil
}

// ExchangeRate returns exchange rate by given currency.
//
// It implements asset.currencyRatesExchanger interface.
func (cs *Service) ExchangeRate(currency currency.Enum) (float64, error) {
	if !cs.rates.Success {
		return 0, errors.New("could not obtain rates")
	}

	return cs.rates.Rates.rate(currency)
}
