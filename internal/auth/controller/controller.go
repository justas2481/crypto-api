// controller package responds to endpoints
// that let user to authenticate.
//
// Process is invoked through handlers as functions.
package controller

import (
	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
)

// Dependencies allow to pass services needed easily so to maintain
// dependency injection.
type dependencies struct {
	Authentication *auth.Authentication
}

// NewDependencies initializes dependencies struct with needed services as its fields.
func NewDependencies(authentication *auth.Authentication) dependencies {
	return dependencies{Authentication: authentication}
}
