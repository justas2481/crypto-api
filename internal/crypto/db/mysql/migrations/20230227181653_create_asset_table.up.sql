BEGIN;

CREATE TABLE IF NOT EXISTS `asset` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT NOT NULL COMMENT 'Which user this asset belongs to',
    `label` VARCHAR (255) NOT NULL COMMENT 'Name of the asset',
    `currency_type` ENUM ('BTC', 'ETH', 'LTC') NOT NULL,
    `currency_amount` DECIMAL (10, 2) NOT NULL COMMENT 'How much such type of asset user owns',
    CONSTRAINT `pk_id` PRIMARY KEY (id),
    CONSTRAINT `chk_currency_amount_positive` CHECK (`currency_amount` > 0)
)
ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE INDEX `idx_label` ON `asset` (`label`);

COMMIT;