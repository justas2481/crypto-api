package http_test

import (
	"context"
	"io"
	"log"
	gohttp "net/http"
	"reflect"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/justas2481/crypto-api/internal/pkg/http"
)

const (
	serverAddr    = "127.0.0.1"
	serverPort    = "1024"
	scheme        = "http"
	serverBaseURL = scheme + "://" + serverAddr + ":" + serverPort + "/"
)

func init() {
	log.SetFlags(0)
	log.SetOutput(io.Discard)
}

func TestNewServer(t *testing.T) {
	routerStub := gohttp.HandlerFunc(func(gohttp.ResponseWriter, *gohttp.Request) {})
	server := http.NewServer(
		routerStub,
		serverAddr,
		serverPort,
		time.Duration(http.DefaultServerReadHeaderTimeout),
		time.Duration(http.DefaultServerWriteTimeout),
	)
	want := "Server"
	got := reflect.TypeOf(*server).Name()
	if got != want {
		t.Fatalf("can not create server instance: want %v; got %v", want, got)
	}
}

func TestStart(t *testing.T) {
	router := mux.NewRouter()
	router.HandleFunc("/", func(w gohttp.ResponseWriter, r *gohttp.Request) {
		w.WriteHeader(gohttp.StatusOK)
		w.Write([]byte("OK"))
	})
	server := http.NewServer(
		router,
		serverAddr,
		serverPort,
		http.DefaultServerReadHeaderTimeout,
		http.DefaultServerWriteTimeout,
	)
	errCh := make(chan error, 1)
	go func() {
		errCh <- server.Start()
	}()
	listenNotify := server.ListeningCh()
	select {
	case err := <-errCh:
		if err != gohttp.ErrServerClosed {
			t.Fatalf("server failed with error: %v", err)
		}
	case <-listenNotify:
		defer server.Shutdown(context.Background())
		resp, err := gohttp.Get(serverBaseURL)
		if err != nil {
			t.Fatalf("can not request started http server endpoint: %v", err)
		}
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			t.Fatal("can not read response body")
		}
		if string(body) != "OK" {
			t.Fatalf("unexpected response from HTTP server: OK expected; got %s", body)
		}
	}

	serverSecond := http.NewServer(
		router,
		serverAddr,
		serverPort,
		http.DefaultServerReadHeaderTimeout,
		http.DefaultServerWriteTimeout,
	)
	errChSecond := make(chan error, 1)
	go func() {
		errChSecond <- serverSecond.Start()
	}()
	listenNotify = serverSecond.ListeningCh()
	select {
	case <-errChSecond:
	case <-listenNotify:
		defer serverSecond.Shutdown(context.Background())
		t.Fatal(
			"second server should not have been started on same network interface and port",
		)
	}
}

func TestListeningCh(t *testing.T) {
	routerStub := gohttp.HandlerFunc(func(gohttp.ResponseWriter, *gohttp.Request) {})
	server := http.NewServer(
		routerStub,
		serverAddr,
		serverPort,
		time.Duration(http.DefaultServerReadHeaderTimeout),
		time.Duration(http.DefaultServerWriteTimeout),
	)
	if server.ListeningCh() == nil {
		t.Error("expected chan got nil")
	}
	if server.ListeningCh() == nil {
		t.Error("expected chan got nil")
	}
}
