package http

import (
	"fmt"
	"io"
	"net/http"
	"time"
)

// Default constants for the client configuration.
const (
	// Request is canseled after no longer than 5 s.
	DefaultClientTimeout int = 5 // In seconds
)

// Client is a wrapper for http Client to make requests to the server.
type Client struct {
	http.Client
}

// NewClient returns instance of the client.
func NewClient(timeout time.Duration) *Client {
	return &Client{http.Client{Timeout: timeout * time.Second}}
}

// Get makes a GET request to the given endpoint
// and returns its full response body.
func (c *Client) Get(endpointUrl string) ([]byte, error) {
	req, err := http.NewRequest(http.MethodGet, endpointUrl, nil)
	if err != nil {
		return []byte{}, fmt.Errorf("instantiating HTTP request: %w", err)
	}
	resp, err := c.Do(req)
	if err != nil {
		return []byte{}, fmt.Errorf("making HTTP request: %w", err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, fmt.Errorf("reading response body: %w", err)
	}

	return body, nil
}
