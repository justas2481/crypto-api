package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/justas2481/crypto-api/internal/crypto/asset"
	"gitlab.com/justas2481/crypto-api/internal/crypto/integration/coinlayer"
	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
	"gitlab.com/justas2481/crypto-api/internal/pkg/ctrl"
)

// CRUD controller for user assets.
type assetController struct {
	ctrl.BaseController
	assetService        asset.Service
	exchangeRateService *coinlayer.Service
}

// NewAsset initializes asset controller.
func newAsset(authorization *auth.Authorization, assetService asset.Service, coinlayerService *coinlayer.Service) *assetController {
	return &assetController{
		BaseController:      ctrl.BaseController{Authorization: authorization},
		assetService:        assetService,
		exchangeRateService: coinlayerService,
	}
}

// createAsset creates new user asset.
func (c *assetController) createAsset(w http.ResponseWriter, r *http.Request) {
	jsonDec := json.NewDecoder(r.Body)
	jsonResp := make(map[string]string)

	claims, err := c.ClaimsFromRequest(r)
	if err != nil {
		jsonResp["err"] = "user_info_not_determined"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	asset := asset.Model{}
	if err = jsonDec.Decode(&asset); err != nil {
		jsonResp["err"] = "resource_badly_formatted"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	assetId, err := c.assetService.CreateAsset(claims.UserId, asset)
	if err != nil {
		log.Println(err)
		jsonResp["err"] = "asset_creation_failed"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	c.JSONResponse(w, http.StatusCreated, map[string]int64{"asset_id": assetId})
}

// getUserAssets returns user assets as a response.
func (c *assetController) getUserAssets(w http.ResponseWriter, r *http.Request) {
	jsonResp := make(map[string]string)

	claims, err := c.ClaimsFromRequest(r)
	if err != nil {
		jsonResp["err"] = "user_info_not_determined"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	assets, err := c.assetService.GetUserAssets(claims.UserId)
	if err != nil {
		log.Println(err)
		jsonResp["err"] = "asset_retreval_failed"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	c.JSONResponse(w, http.StatusOK, assets)
}

// updateAsset takes Asset as JSON request and persists changes to storage.
func (c *assetController) updateAsset(w http.ResponseWriter, r *http.Request) {
	jsonDec := json.NewDecoder(r.Body)
	jsonResp := make(map[string]string)

	claims, err := c.ClaimsFromRequest(r)
	if err != nil {
		jsonResp["err"] = "user_info_not_determined"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	asset := asset.Model{}
	if err = jsonDec.Decode(&asset); err != nil {
		jsonResp["err"] = "resource_badly_formatted"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	assetId, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		jsonResp["err"] = "no_asset_id_provided"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	if err = c.assetService.UpdateAsset(claims.UserId, assetId, asset); err != nil {
		log.Println(err)
		jsonResp["err"] = "asset_update_failed"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	c.JSONResponse(w, http.StatusNoContent, nil)
}

// deleteAsset removes user's asset by Asset ID.
func (c *assetController) deleteAsset(w http.ResponseWriter, r *http.Request) {
	jsonResp := make(map[string]string)

	claims, err := c.ClaimsFromRequest(r)
	if err != nil {
		jsonResp["err"] = "user_info_not_determined"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	assetId, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		jsonResp["err"] = "no_asset_id_provided"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	if err = c.assetService.RemoveAsset(assetId, claims.UserId); err != nil {
		log.Println(err)
		jsonResp["err"] = "asset_update_failed"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	c.JSONResponse(w, http.StatusNoContent, nil)
}

// getAssetValue returns value of the given asset in dollars.
func (c *assetController) getAssetValue(w http.ResponseWriter, r *http.Request) {
	jsonResp := make(map[string]string)
	claims, err := c.ClaimsFromRequest(r)
	if err != nil {
		jsonResp["err"] = "user_info_not_determined"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	assetId, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		log.Println(err)
		jsonResp["err"] = "asset_id_badly_formatted"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	name, val, err := c.assetService.ValueInUSD(claims.UserId, assetId)
	if err != nil {
		log.Println(err)
		jsonResp["err"] = "rates_retreval_failed"
		c.JSONResponse(w, http.StatusInternalServerError, jsonResp)
		return
	}

	c.JSONResponse(w, http.StatusOK, struct {
		Name string  `json:"name"`
		Val  float64 `json:"val"`
	}{Name: name, Val: val},
	)
}

// getAssetsValues provides a JSON response to request
// for sum of assets owned by user in dollars.
func (c *assetController) getAssetsValues(w http.ResponseWriter, r *http.Request) {
	jsonResp := make(map[string]string)
	claims, err := c.ClaimsFromRequest(r)
	if err != nil {
		jsonResp["err"] = "user_info_not_determined"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	assetsSum, err := c.assetService.SumValuesInUSD(claims.UserId)
	if err != nil {
		log.Println(err)
		jsonResp["err"] = "currency_values__retreval_failed"
		c.JSONResponse(w, http.StatusInternalServerError, jsonResp)
		return
	}

	c.JSONResponse(w, http.StatusOK, map[string]float64{"total": assetsSum})
}
