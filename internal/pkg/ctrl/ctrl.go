// ctrl package gives a base functionality for retrieving claims from request,
// provides helpers to respond with statuses
// and implements default handlers for unmached routes and request methods.
//
// Main power of this package is the BaseController struct
// that can be embedded to other controllers to extend their functionality.
package ctrl

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
)

// notFound type represents handler to respond to unmatched routes.
type NotFoundHandler func(http.ResponseWriter, *http.Request)

// MethodNotAllowedHandler type represents handler to respond
// to unmatched request methods of matched routes.
type MethodNotAllowedHandler func(http.ResponseWriter, *http.Request)

// BaseController is the struct to be embedded to the other controllers
// that need helpers that base controller provides.
type BaseController struct {
	Authentication *auth.Authentication
	Authorization  *auth.Authorization
}

// NewBaseController initializes bace controller instance.
func NewBaseController(authentication *auth.Authentication, authorization *auth.Authorization) *BaseController {
	return &BaseController{Authentication: authentication, Authorization: authorization}
}

// claimsFromRequest obtains claim from request header
// and returns claims from unverified token.
//
// This helper function is useful to handlers that can be accessed only
// when JWT is already been verified.
func (bc *BaseController) ClaimsFromRequest(r *http.Request) (*auth.Claims, error) {
	return bc.Authorization.ClaimsFromUnverifiedToken(
		auth.ParseTokenAsBearer(
			r.Header.Get("authorization"),
		),
	)
}

// MustAuthorize insures that only authorized user can access
// resources that are private.
//
// Private resource is called as handler functions.
func (bc *BaseController) MustAuthorize(handleFunc http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			jsonEnc := json.NewEncoder(w)
			jsonResp := make(map[string]string)

			token := r.Header.Get("authorization")
			token = auth.ParseTokenAsBearer(token)
			_, err := bc.Authorization.VerifyToken(token, bc.Authentication.SecretKey())
			if err != nil {
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				jsonResp["err"] = "unknown_error"
				if errors.Is(err, auth.ErrJWTInvalid) {
					jsonResp["err"] = "unauthorized"
				}

				jsonEnc.Encode(jsonResp)
				return
			}

			handleFunc(w, r)
		})
}

// JSONResponse responds with appropriate JSON header,
// http status code and JSON payload.
//
// If error occurs when encoding payload,
// http.StatusInternalServerError will be sent
// with headers no matter the actual statusCode set.
func (bc *BaseController) JSONResponse(w http.ResponseWriter, statusCode int, payload any) error {
	w.Header().Set("Content-Type", "application/json")
	jsonEnc := json.NewEncoder(w)
	if err := jsonEnc.Encode(payload); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return errors.New("can not encode JSON payload")
	}

	return nil
}

// status returns a function as a provided status handler
// that writes status to the HTTP header
// and sends JSON output to the client that made a request.
//
// It is used to indicate some sort of error
// and respond with HTTP status + JSON to inform the caller.
func status(code int) http.HandlerFunc {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("content-type", "application/json")
			w.WriteHeader(code)
			jsonEnc := json.NewEncoder(w)
			jsonResp := make(map[string]int)
			jsonResp["err"] = code
			jsonEnc.Encode(jsonResp)
		})
}

// NotFoundHandler deals with request that does not match any given route.
func (NotFoundHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	stat := status(http.StatusNotFound)
	stat(w, r)
}

// methodNotAllowedHandler deals with request that does not match any
// supported request method for a particular route.
func (MethodNotAllowedHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	stat := status(http.StatusMethodNotAllowed)
	stat(w, r)
}
