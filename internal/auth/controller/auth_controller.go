package controller

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
	"gitlab.com/justas2481/crypto-api/internal/pkg/ctrl"
)

// Auth controller for authentication.
type authController struct {
	ctrl.BaseController
}

// NewAuth initializes controller object for authentication.
func newAuth(authentication *auth.Authentication) *authController {
	return &authController{
		BaseController: ctrl.BaseController{Authentication: authentication},
	}
}

// authenticate takes user credentials from request
// and returns response as newly issued JWT token on success,
// or some JSON error on failure.
func (c *authController) authenticate(w http.ResponseWriter, r *http.Request) {
	jsonDec := json.NewDecoder(r.Body)
	jsonResp := make(map[string]string)

	var crd auth.Credentials
	if err := jsonDec.Decode(&crd); err != nil {
		jsonResp["err"] = "credentials_badly_formatted"
		c.JSONResponse(w, http.StatusBadRequest, jsonResp)
		return
	}

	tokenString, expires, err := c.Authentication.GenerateToken(crd)
	if errors.Is(err, auth.ErrInvalidCredentials) {
		jsonResp["err"] = "credentials_invalid"
		c.JSONResponse(w, http.StatusUnauthorized, jsonResp)
		return
	} else if err != nil {
		jsonResp["err"] = "can_not_generate_token"
		c.JSONResponse(w, http.StatusInternalServerError, jsonResp)
		return
	}

	token := auth.JSONToken{Token: tokenString, Expires: expires}
	c.JSONResponse(w, http.StatusOK, token)
}
