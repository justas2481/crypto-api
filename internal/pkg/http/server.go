package http

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"time"
)

// Server configuration suggested defaults.
// Useful to avoid magical numbers in code.
const (
	DefaultServerAddress           = "127.0.0.1"
	DefaultServerPort              = "8000"
	DefaultServerReadHeaderTimeout = 3 // Seconds
	DefaultServerWriteTimeout      = 3 // Seconds
)

// Server that embeds http.Server.
//
// This makes easier to deal with timeouts and other configurations.
type Server struct {
	http.Server

	// If channel is not nil, Server.Start sends enpty struct
	// onto the channel to the caller to notify that server is listening.
	//
	// Primarily it is used for black-box testing
	// to inform the test function of the current server state.
	//
	// Channel itself should be a buffered channel with capacity of 1.
	//
	// This channel is returned to the external packaged by invoking ListeningCh method.
	listenerNotifyCh chan struct{}
}

// NewServer instantiates server struct with options given.
func NewServer(
	router http.Handler,
	host, port string,
	readHeaderTimeout, writeTimeout time.Duration,
) *Server {
	return &Server{
		Server: http.Server{
			Addr:              host + ":" + port,
			Handler:           router,
			ReadHeaderTimeout: readHeaderTimeout * time.Second,
			WriteTimeout:      writeTimeout * time.Second,
		},
		listenerNotifyCh: nil,
	}
}

// Start starts the server.
//
// server runs the API service, listens for requests and provides responses.
//
// If server can not be started, it returns an error.
//
// If server is closed, program is stopped and 0 as error code is returned to the system.
//
// ListenNotify channel informs the caller
// about the successful network listening process
// being invoked if that happens without errors.
// Channel itself must be a buffered channel with capacity of 1
// to let server proceed if the caller is
// not ready to immediately receive the message.
// Channel is ignored and message is not sent if channel is nil.
//
// It uses log package to log things.
func (s *Server) Start() error {
	log.Println("Starting server...")
	listener, err := net.Listen("tcp", s.Addr)
	if err != nil {
		return fmt.Errorf("server is unable to listen for connections: %w", err)
	}
	if s.listenerNotifyCh != nil {
		s.listenerNotifyCh <- struct{}{}
	}

	log.Println("Server is exposing services...")
	return s.Serve(listener)
}

// ListeningCh returns the channel which is used by the server
// to inform about the state of a listener when it successfully starts.
func (s *Server) ListeningCh() <-chan struct{} {
	if s.listenerNotifyCh != nil {
		return s.listenerNotifyCh
	}

	s.listenerNotifyCh = make(chan struct{}, 1)

	return s.listenerNotifyCh
}
