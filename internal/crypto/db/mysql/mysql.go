// mysql manages initialization of a MySQL database and connection to it.
package mysql

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

// ConnConf for the database connection.
type ConnConf struct {
	DBDriver string // Database driver to be used, i.e. mysql.
	User     string // DB user.
	Password string // DB password.
	Net      string // Network to be used, such as TCP.
	Addr     string // IP address that is used to connect to DB, such as 127.0.0.1.
	Port     string // Port number that is to be used when connecting, such as 3306.
	DBName   string // Name for the database, such as crypto.
}

// CustomConfiger defines permitted types for custom particular DB configurations.
//
// It utilizes generics to avoid many different functions to be created
// just for the configuration differences.
type CustomConfiger interface {
	CustomConf // For now only one datatype allowed.
}

// Mysql native configurations.
type CustomConf struct {
	AllowNativePasswords bool // Use native passwords auth mechanism or not.
}

// Connect initializes database struct, pings it
// and returns pointer to database struct
// with initialized DB resources according to passed driver.
//
// If unsupported driver is passed, function panics.
//
// CustomConf can be passed
// to set specified configuration options to the given database driver.
//
// TODO: check if generics syntax is must.
func Connect[C CustomConfiger](conf ConnConf, customConf C) (*sql.DB, error) {
	var dsn string
	var dbH *sql.DB

	switch conf.DBDriver {
	case "mysql":
		if conf.Net == "" {
			conf.Net = "tcp"
		}
		conf.Addr = conf.Addr + ":" + conf.Port
		customCfg, ok := any(customConf).(CustomConf)
		if !ok {
			return nil, errors.New("can not set custom config for Mysql")
		}

		cfg := mysql.Config{
			User:                 conf.User,
			Passwd:               conf.Password,
			Net:                  conf.Net,
			Addr:                 conf.Addr,
			DBName:               conf.DBName,
			AllowNativePasswords: customCfg.AllowNativePasswords,
		}

		dsn = cfg.FormatDSN()
		var err error
		dbH, err = sql.Open(conf.DBDriver, dsn)
		if err != nil {
			return nil, fmt.Errorf("creating mysql database connection: %w", err)
		}
		pingErr := dbH.Ping()
		if pingErr != nil {
			defer dbH.Close()
			return nil, fmt.Errorf("pinging to mysql database: %w", err)
		}

	default:
		panic("unknown database driver name passed.")
	}

	return dbH, nil
}
