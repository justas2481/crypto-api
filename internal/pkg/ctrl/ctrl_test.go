package ctrl_test

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
	"gitlab.com/justas2481/crypto-api/internal/pkg/ctrl"
)

const (
	credentialsUsername = "test1"
	credentialsPassword = "test1pwd"
	userId              = 1
)

func baseControllerAndToken(t *testing.T, tokenExpires bool) (baseController *ctrl.BaseController, token string) {
	authentication := auth.NewAuthentication([]byte(auth.DefaultJWTKey), auth.Validation{})
	if tokenExpires {
		authentication.SetDuration(-1 * time.Hour)
	}
	token, _, err := authentication.GenerateToken(auth.Credentials{Username: credentialsUsername, Password: credentialsPassword})
	if err != nil {
		t.Fatal("token should be generated successfully")
	}
	return ctrl.NewBaseController(authentication, auth.NewAuthorization()), token
}

func TestClaimsFromRequest(t *testing.T) {
	bc, token := baseControllerAndToken(t, false)
	req := httptest.NewRequest("GET", "/", nil)
	req.Header.Set("authorization", "bearer fake-token")
	if _, err := bc.ClaimsFromRequest(req); err == nil {
		t.Fatal("expected error; got nil")
	}
	req.Header.Set("authorization", "bearer "+token)
	claims, err := bc.ClaimsFromRequest(req)
	if err != nil {
		t.Fatalf("expected no error; got %v", err)
	}
	if claims.UserId != userId {
		t.Fatalf("expected user id %v; got %v", userId, claims.UserId)
	}
}

func TestMustAuthorize(t *testing.T) {
	bc, token := baseControllerAndToken(t, false)
	_, expiredToken := baseControllerAndToken(t, true)
	tt := map[string]struct {
		token      string
		wantStatus int
		wantBody   string
	}{
		"token with invalid signature": {"invalid-token", http.StatusUnauthorized, `{"err":"unauthorized"}`},
		"expired token":                {expiredToken, http.StatusUnauthorized, `{"err":"unauthorized"}`},
		"authorized request":           {token, http.StatusOK, `{"status":"OK"}`},
	}
	for name, tc := range tt {
		t.Run(name, func(t *testing.T) {
			hFunc := bc.MustAuthorize(
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusOK)
					w.Write([]byte(`{"status":"OK"}` + "\n"))
				}),
			)
			req := httptest.NewRequest("GET", "/", nil)
			req.Header.Set("authorization", "bearer "+tc.token)
			rr := httptest.NewRecorder()
			tc.wantBody += "\n"
			hFunc.ServeHTTP(rr, req)
			if rr.Code != tc.wantStatus {
				t.Fatalf("expected status %v; got %v", tc.wantStatus, rr.Code)
			}
			gotBody := rr.Body.String()
			if gotBody != tc.wantBody {
				t.Fatalf("expected %q; got %q", tc.wantBody, gotBody)
			}
		})
	}
}

func TestJSONResponse(t *testing.T) {
	authentication := auth.NewAuthentication([]byte(auth.DefaultJWTKey), auth.Validation{})
	bc := ctrl.NewBaseController(authentication, auth.NewAuthorization())
	rr := httptest.NewRecorder()
	if err := bc.JSONResponse(rr, http.StatusOK, make(chan struct{})); err == nil {
		t.Fatal(
			"expected error so to not be able to encode JSON; got nil",
		)
	}
	wantContentType := "application/json"
	gotContentType := rr.Header().Get("content-type")
	wantStatus := http.StatusInternalServerError
	gotStatus := rr.Code
	if gotContentType != wantContentType {
		t.Fatalf("want content type: %q; got %q", wantContentType, gotContentType)
	}
	if gotStatus != wantStatus {
		t.Fatalf("want status: %d; got %d", wantStatus, gotStatus)
	}
	rr = httptest.NewRecorder()
	if err := bc.JSONResponse(rr, http.StatusOK, `{"status":"OK"}`+"\n"); err != nil {
		t.Fatalf("has to be no error; got %v", err)
	}
	wantContentType = "application/json"
	gotContentType = rr.Header().Get("content-type")
	wantStatus = http.StatusOK
	gotStatus = rr.Code
	if gotContentType != wantContentType {
		t.Fatalf("want content type: %q; got %q", wantContentType, gotContentType)
	}
	if gotStatus != wantStatus {
		t.Fatalf("want status: %d; got %d", wantStatus, gotStatus)
	}
}

func TestNotFoundHandlerServeHTTP(t *testing.T) {
	req := httptest.NewRequest("GET", "/", nil)
	rr := httptest.NewRecorder()
	var nfh ctrl.NotFoundHandler
	nfh.ServeHTTP(rr, req)
	wantContentType := "application/json"
	gotContentType := rr.Header().Get("content-type")
	if gotContentType != wantContentType {
		t.Fatalf("unexpected content type: want %q; got %q", wantContentType, gotContentType)
	}
	if rr.Code != http.StatusNotFound {
		t.Fatalf("expected status %d; got %d", http.StatusNotFound, rr.Code)
	}
	wantBody := `{"err":404}` + "\n"
	gotBody := rr.Body.String()
	if gotBody != wantBody {
		t.Fatalf("unexpected body: expected %q; got %q", wantBody, gotBody)
	}
}

func TestMethodNotAllowedHandlerServeHTTP(t *testing.T) {
	req := httptest.NewRequest("GET", "/", nil)
	rr := httptest.NewRecorder()
	var nfh ctrl.MethodNotAllowedHandler
	nfh.ServeHTTP(rr, req)
	wantContentType := "application/json"
	gotContentType := rr.Header().Get("content-type")
	if gotContentType != wantContentType {
		t.Fatalf("unexpected content type: want %q; got %q", wantContentType, gotContentType)
	}
	if rr.Code != http.StatusMethodNotAllowed {
		t.Fatalf("expected status %d; got %d", http.StatusMethodNotAllowed, rr.Code)
	}
	wantBody := `{"err":405}` + "\n"
	gotBody := rr.Body.String()
	if gotBody != wantBody {
		t.Fatalf("unexpected body: expected %q; got %q", wantBody, gotBody)
	}
}
