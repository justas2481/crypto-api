package controller

import (
	"github.com/gorilla/mux"
	"gitlab.com/justas2481/crypto-api/internal/pkg/ctrl"
)

// SetRoutes sets the routes to the router,
// invokes controllers and passes dependencies to them.
func SetRoutes(router *mux.Router, dep dependencies) {
	var notFound ctrl.NotFoundHandler
	var methodNotAllowed ctrl.MethodNotAllowedHandler
	router.NotFoundHandler = notFound
	router.MethodNotAllowedHandler = methodNotAllowed

	auth := newAuth(dep.Authentication)
	router.HandleFunc("/authentication", auth.authenticate).Methods("POST")
}
