module gitlab.com/justas2481/crypto-api

go 1.19

require (
	github.com/golang-jwt/jwt/v5 v5.0.0-rc.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.5.1
)

require github.com/go-sql-driver/mysql v1.7.0
