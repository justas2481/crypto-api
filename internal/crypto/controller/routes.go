package controller

import (
	"github.com/gorilla/mux"
	"gitlab.com/justas2481/crypto-api/internal/crypto/asset"
	"gitlab.com/justas2481/crypto-api/internal/crypto/integration/coinlayer"
	"gitlab.com/justas2481/crypto-api/internal/pkg/ctrl"
)

// SetRoutes sets the routes to the router,
// invokes controllers and passes dependencies to them.
func SetRoutes(router *mux.Router, dep dependencies) {
	var notFound ctrl.NotFoundHandler
	var methodNotAllowed ctrl.MethodNotAllowedHandler
	router.NotFoundHandler = notFound
	router.MethodNotAllowedHandler = methodNotAllowed

	base := ctrl.NewBaseController(dep.authentication, dep.authorization)

	home := newHome()
	router.HandleFunc("/", home.index).Methods("GET")

	assetRepo := asset.NewMysqlRepository(dep.db)
	coinlayerService := coinlayer.NewService(dep.client, dep.coinlayerApiKey)
	assetService := asset.NewService(assetRepo, coinlayerService)
	asset := newAsset(dep.authorization, assetService, coinlayerService)
	router.HandleFunc("/assets", base.MustAuthorize(asset.createAsset)).Methods("POST")
	router.HandleFunc("/assets", base.MustAuthorize(asset.getUserAssets)).Methods("GET")
	router.HandleFunc("/assets/{id:[0-9]+}", base.MustAuthorize(asset.updateAsset)).Methods("PATCH")
	router.HandleFunc("/assets/{id:[0-9]+}", base.MustAuthorize(asset.deleteAsset)).Methods("DELETE")
	router.HandleFunc("/assets/{id:[0-9]+}/value", base.MustAuthorize(asset.getAssetValue)).Methods("GET")
	router.HandleFunc("/assets/value", base.MustAuthorize(asset.getAssetsValues)).Methods("GET")
}
