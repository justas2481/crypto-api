// Main entry for the Crypto Folio API.
package main

import (
	"log"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/justas2481/crypto-api/internal/crypto/controller"
	"gitlab.com/justas2481/crypto-api/internal/crypto/db/mysql"
	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
	"gitlab.com/justas2481/crypto-api/internal/pkg/envconv"
	"gitlab.com/justas2481/crypto-api/internal/pkg/http"
)

func main() {
	log.Println("Starting Crypto API service.")
	err := godotenv.Load("../default_conf.env")
	if err != nil {
		log.Fatal("Error: can not initialize default configuration environment variables.")
	}
	err = godotenv.Load("server.env")
	if err != nil {
		log.Fatal("Error: can not initialize server environment variables.")
	}
	err = godotenv.Load("database.env")
	if err != nil {
		log.Fatal("Error: can not initialize database environment variables.")
	}
	err = godotenv.Load("coinlayer.env")
	if err != nil {
		log.Fatal("Error: can not initialize Coinlayer environment variables.")
	}

	log.Println("Connecting to the database...")
	cfg := mysql.ConnConf{
		DBDriver: "mysql",
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Addr:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		DBName:   os.Getenv("DB_NAME"),
	}
	customCfg := mysql.CustomConf{AllowNativePasswords: true}
	dbH, err := mysql.Connect(cfg, customCfg)
	if err != nil {
		log.Fatal(err)
	}

	secretKey := []byte(envconv.GetString(os.Getenv("CRYPTO_JWT_KEY"), auth.DefaultJWTKey))
	authentication := auth.NewAuthentication(secretKey, auth.Validation{})
	client := http.NewClient(time.Duration(envconv.GetInt(os.Getenv("HTTP_CLIENT_REQUEST_TIMEOUT"), http.DefaultClientTimeout)))
	coinlayerApiKey := os.Getenv("COINLAYER_API_KEY")
	router := mux.NewRouter().PathPrefix("/api").Subrouter()
	dep := controller.NewDependencies(
		dbH, authentication, auth.NewAuthorization(), client, coinlayerApiKey,
	)
	controller.SetRoutes(router, dep)

	server := http.NewServer(
		router,
		envconv.GetString(os.Getenv("HTTP_HOST"), http.DefaultServerAddress),
		envconv.GetString(os.Getenv("HTTP_PORT"), http.DefaultServerPort),
		time.Duration(envconv.GetInt(os.Getenv("HTTP_READ_HEADER_TIMEOUT"), http.DefaultServerReadHeaderTimeout)),
		time.Duration(envconv.GetInt(os.Getenv("HTTP_WRITE_TIMEOUT"), http.DefaultServerWriteTimeout)),
	)
	log.Fatal(server.Start())
}
