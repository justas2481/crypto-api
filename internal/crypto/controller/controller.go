// controller package deals with different API endpoints
// through handlers as functions.
package controller

import (
	"database/sql"

	"gitlab.com/justas2481/crypto-api/internal/pkg/auth"
	"gitlab.com/justas2481/crypto-api/internal/pkg/http"
)

// Dependencies allow to pass services needed easily so to maintain
// dependency injection.
type dependencies struct {
	db              *sql.DB
	authentication  *auth.Authentication
	authorization   *auth.Authorization
	client          *http.Client
	coinlayerApiKey string
}

// NewDependencies initializes dependencies struct with needed services as its fields.
//
// If db is nil, function panics.
func NewDependencies(
	db *sql.DB,
	authentication *auth.Authentication,
	authorization *auth.Authorization,
	client *http.Client,
	coinlayerApiKey string,
) dependencies {
	if db == nil {
		panic("database is set to nil")
	}

	return dependencies{
		db:              db,
		authentication:  authentication,
		authorization:   authorization,
		client:          client,
		coinlayerApiKey: coinlayerApiKey,
	}
}
