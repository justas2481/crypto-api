package asset

import (
	"database/sql"
	"errors"
	"fmt"
)

// MysqlRepository implements Repository interface of the Asset service.
type MysqlRepository struct {
	DB *sql.DB // Mysql database handle.
}

// NewMysqlRepository initializes MysqlAssetRepository struct
// and returns pointer to it.
func NewMysqlRepository(db *sql.DB) *MysqlRepository {
	return &MysqlRepository{DB: db}
}

// Insert inserts Asset entry into the database
// and returns newly inserted Asset ID.
func (mr MysqlRepository) Insert(userId int64, asset Model) (id int64, err error) {
	tx, err := mr.DB.Begin()
	if err != nil {
		return 0, fmt.Errorf("beginning transaction for new asset: %w", err)
	}
	defer tx.Rollback()
	res, err := tx.Exec(
		"INSERT INTO asset ( `user_id`, `label`, `currency_type`, `currency_amount`)"+
			"VALUES (?, ?, ?, ?)",
		userId, asset.Label, asset.Type, asset.Amount,
	)
	if err != nil {
		return 0, fmt.Errorf("inserting asset into database: %w", err)
	}
	id, err = res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("obtaining last insert asset id: %w", err)
	}
	if err := tx.Commit(); err != nil {
		return 0, fmt.Errorf("commiting tx for new asset: %w", err)
	}

	return id, nil
}

// Select gets all the assets from DB that belong to a given user.
func (mr MysqlRepository) Select(
	userId int64,
	offset, limit uint,
	orderByFields map[string]bool,
) ([]Model, error) {
	params := make([]interface{}, 0, 3)
	params = append(params, userId)
	sql :=
		"SELECT `id`, `label`, `currency_type`, `currency_amount` FROM asset WHERE `user_id` = ?"

	if len(orderByFields) > 0 {
		sql += " ORDER BY"
		var count int
		for field, asc := range orderByFields {
			count++
			if field == "" || !alphaUnderscore(field) {
				return nil, errors.New(
					"reading assets: invalid field name for sorting values with order by",
				)
			}
			var order, sep string
			if asc {
				order = "ASC"
			} else {
				order = "DESC"
			}
			if count < len(orderByFields) {
				sep = ","
			}
			sql += " " + field + " " + order + sep
		}
	}

	if limit > 0 {
		params = append(params, offset, limit)
		sql += " LIMIT ?, ?"
	}

	rows, err := mr.DB.Query(sql, params...)
	if err != nil {
		return nil, fmt.Errorf("querying for user assets: %w", err)
	}
	defer rows.Close()

	var assets []Model
	for rows.Next() {
		var asset Model
		asset.UserId = userId
		if err := rows.Scan(&asset.Id, &asset.Label, &asset.Type, &asset.Amount); err != nil {
			return assets, fmt.Errorf("populating user assets to fields: %w", err)
		}
		assets = append(assets, asset)
	}
	if err = rows.Err(); err != nil {
		return assets, fmt.Errorf("trying to query for user assets: %w", err)
	}

	return assets, nil
}

// Update updates asset record in the database by its own and user's ID.
func (mr MysqlRepository) Update(assetId int64, asset Model) error {
	// Cap just for optimisation only (3 struct fields for now) + 2 fields in WHERE clause,
	// but optimisation like this makes harder to refactor things.
	//
	// TODO: consider to remove optimisation, add considerably larger cap,
	// or even make size = cap.
	params := make([]interface{}, 0, 5)

	sql :=
		"UPDATE `asset` SET"
	// TODO: think of this boilerplate.
	if asset.Label != nil {
		params = append(params, asset.Label)
		sql += " `label` = ?,"
	}
	if asset.Type != nil {
		params = append(params, asset.Type)
		sql += " `currency_type` = ?,"
	}
	if asset.Amount != nil {
		params = append(params, asset.Amount)
		sql += " `currency_amount` = ?,"
	}
	if len(params) < 1 {
		return errors.New("updating asset: there is nothing to update")
	}
	sql = sql[0 : len(sql)-1] // Remove last comma (,).
	params = append(params, assetId, asset.UserId)
	sql += " WHERE `id` = ? AND `user_id` = ? LIMIT 1"
	if _, err := mr.DB.Exec(sql, params...); err != nil {
		return fmt.Errorf("updating asset: %w", err)
	}

	return nil
}

// Delete Asset from DB by its ID.
func (mr MysqlRepository) Delete(assetId int64, userId int64) error {
	sql := "DELETE FROM `asset` WHERE `id` = ? AND `user_id` = ?"
	if _, err := mr.DB.Exec(sql, assetId, userId); err != nil {
		return fmt.Errorf("deleting asset: %w", err)
	}

	return nil
}

// Find an asset by ID.
func (mr MysqlRepository) SelectById(assetId int64, userId int64) (Model, error) {
	sql := "SELECT `id`, `user_id`, `label`, `currency_type`, `currency_amount`"
	sql += " FROM `asset`"
	sql += " WHERE `id` = ? AND `user_id` = ? LIMIT 1"
	var asset Model
	row := mr.DB.QueryRow(sql, assetId, userId)
	if err := row.Scan(&asset.Id, &asset.UserId, &asset.Label, &asset.Type, &asset.Amount); err != nil {
		return asset, fmt.Errorf("querying for asset by id: %w", err)
	}

	return asset, nil
}
