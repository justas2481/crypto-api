package http_test

import (
	"errors"
	"io"
	gohttp "net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"gitlab.com/justas2481/crypto-api/internal/pkg/http"
)

// See https://stackoverflow.com/questions/71632844/can-i-write-my-own-version-of-gos-http-client-do-function-for-my-test-cases
var errReadFail = errors.New("reading failed")

type readerStub int

func (rs readerStub) Read([]byte) (int, error) {
	return 0, errReadFail
}

type transportStub struct {
	body io.Reader
}

func (ts *transportStub) RoundTrip(req *gohttp.Request) (*gohttp.Response, error) {
	return &gohttp.Response{
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 0,
		Header:     make(gohttp.Header),
		Close:      true,
		Body:       io.NopCloser(ts.body),
	}, nil
}

func TestNewClient(t *testing.T) {
	client := http.NewClient(time.Duration(http.DefaultClientTimeout))
	want := "Client"
	got := reflect.TypeOf(*client).Name()
	if got != want {
		t.Fatalf("can not create client instance: want %v; got %v", want, got)
	}
}

func TestGet(t *testing.T) {
	client := http.NewClient(time.Duration(http.DefaultClientTimeout))
	if _, err := client.Get(string([]byte{0})); err == nil {
		t.Fatal("client should not perform a request with fake URL")
	}
	if _, err := client.Get(serverBaseURL); err == nil {
		t.Fatal("client makes request successfully though server should not be active")
	}
	server := httptest.NewServer(
		gohttp.HandlerFunc(func(w gohttp.ResponseWriter, r *gohttp.Request) {
			w.WriteHeader(gohttp.StatusOK)
			w.Write([]byte("OK"))
		}),
	)
	defer server.Close()
	client.Transport = &transportStub{body: readerStub(0)}
	_, err := client.Get(server.URL)
	if err == nil {
		t.Error("request should not have been successful due to the fake reader")
	}
	client.Transport = gohttp.DefaultTransport
	body, err := client.Get(server.URL)
	if err != nil {
		t.Fatalf("request should be successful: %v", err)
	}
	if string(body) != "OK" {
		t.Fatalf("body \"OK\" expected; got %s", body)
	}
}
