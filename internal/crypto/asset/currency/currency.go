// currency package generalizes the currency types across crypto system.
package currency

import (
	"database/sql/driver"
	"errors"
)

// Enum for currency type.
//
// Since Go has no enums, it is done by the help of constants,
// ability to create a datatype from primitives and because of the interfaces
// that are used to convert DB datatypes to Go datatypes.
type Enum string

// Different possible currency types.
const (
	BTC Enum = "BTC"
	ETH Enum = "ETH"
	LTC Enum = "LTC"
)

// Scan implements sql.Scanner interface
// so to allow reading value from enum field in DB.
func (c *Enum) Scan(value any) error {
	val, ok := value.([]byte)
	if !ok {
		return errors.New("scan source not of type []byte")
	}

	*c = Enum(string(val))

	return nil
}

// Value implements sql/driver.Valuer interface.
//
// It converts currencyEnum to string,
// suitable for enum type in DB.
func (c Enum) Value() (driver.Value, error) {
	if !c.isEnumItemValid() {
		return nil, errors.New(
			"enum value is not suitable for conversion to string enum (invalid item)",
		)
	}

	return string(c), nil
}

// isEnumItemValid checks whether value of an enum is one of the enum items.
func (c Enum) isEnumItemValid() bool {
	return c == BTC || c == ETH || c == LTC
}
